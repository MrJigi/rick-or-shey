using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallWallBoost : MonoBehaviour
{
   public Rigidbody wallstuff;
    Vector3 lastVelocity;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        lastVelocity = wallstuff.velocity;
    }

    void OnCollisionEnter(Collision coll)
    {
        
        if (coll.gameObject.tag == "Wall")
        {
            Debug.Log("Object hit Wall");
            var speed = lastVelocity.magnitude;
            wallstuff = coll.gameObject.GetComponent<Rigidbody>();
            var direction = Vector3.Reflect(lastVelocity.normalized, coll.contacts[0].normal);
            wallstuff.velocity = direction * Mathf.Max(speed, 0f);
            // wallstuff.AddForce(wallstuff.position * -1000);
        }
    }


}
