using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootingMechanics : MonoBehaviour
{
    [SerializeField]
    public GameObject bullet;
    GameObject bulletCloneInstance;
    InputManager inputs;
    public Input gameplayActions;
    public InputManager controllerAction;
    public Camera cam;
    public Camera bulletTime;
    

    public Canvas crosshair;
   

    public Transform FirePoint;
    public Vector3 destination;
    public Vector3 offset;
    


    //Camera
    public float smoothSpeed = 0.125f;
    //
    public float bulletRottationSpeed = 50f;
    public float bulletSpeed = 50f;
    public float bulletDeathTimer = 5f;
    public float bulletAccelerationPointSpeed = 20f;
    // Start is called before the first frame update
    private void Awake()
    {
        inputs = GetComponent<InputManager>();
    }
    void Start()
    {
        //crosshair = bulletClone.transform.GetChild(1).GetComponent("Camera") as Camera;
        // bullet = GameObject.Find("Sphere");//GetComponent<GameObject>();
        //bullet = gameObject.GetComponent<GameObject>();
    }

    // Update is called once per frame
    void Update()
    {
        
        MyInput();
    }

    

    private void LateUpdate()
    {
        //HandleCameraOnBullet();
      /*  Vector3 desiredPosition = bulletTime.transform.position + offset;
//Idk about transform position wtf is that
        Vector3 smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, smoothSpeed);
        transform.position = smoothedPosition;*/
    }

    private void MyInput()
    {

        /*if (Input.GetKeyDown(KeyCode.Mouse0))
        {

            bulletInstance = Instantiate(theBullet, GunTrigger.position, GunTrigger.rotation) as Rigidbody;
            bulletInstance.AddForce(GunTrigger.forward * bulletSpeed, ForceMode.Acceleration);
            bulletItems.Add(bulletInstance);
        }*/
        if (Input.GetKeyDown(KeyCode.Mouse1) )
        {
            ShootWherePointing();
        }
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            if (bulletCloneInstance == null)
            {
                //ShootRayWithBullet();
                ShootProjectile();
            }

        }

    }

    void ShootWherePointing()
    {
        
        RaycastHit hitInfo;
        if (Physics.Raycast(cam.ScreenPointToRay(Input.mousePosition), out hitInfo))
        {
            Rigidbody bulletRigidbody = bulletCloneInstance.GetComponent<Rigidbody>();
            Camera bulletCameraThing = bulletCloneInstance.GetComponentInChildren<Camera>();
            //gets the ray where you are pointing to the distance of 
            Ray rayView = bulletCameraThing.ScreenPointToRay(Input.mousePosition);
            Vector3 direction = (rayView.GetPoint(10000.0f)).normalized;

            //Vector3.Reflect(direction, hit.normal);
            // transform.rotation = Quaternion.Euler(0f, 0f, 3 + (-90f));

            Debug.Log("direction of bullet" + direction);
            //50 for some kind of bullet impulse
            bulletRigidbody.AddForce((direction * bulletAccelerationPointSpeed), ForceMode.Impulse);
            // Ray hit something in scene
            Debug.Log("Ray hit : " + hitInfo.collider.gameObject.name);
        }
    }

    void makeTheEnemyRag()
    {
        
    }
    void ShootProjectile()
    {
        //Checks the point with a ray from a camera position
        Ray ray = cam.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
        RaycastHit hit;

        if(Physics.Raycast(ray, out hit))
        {

            destination = hit.point;
            /*InstantiateProjectile(FirePoint);*/
            Ray rayView = cam.ScreenPointToRay(crosshair.transform.position);
            Vector3 vecToPlayer = FirePoint.transform.position - hit.transform.position;
            // Ray hit something in scene
            // Debug.Log("Ray hit : " + hitInfo.collider.gameObject.name);
            GameObject bullet = Instantiate(this.bullet, FirePoint.transform.position, FirePoint.transform.rotation) as GameObject;
            bulletCloneInstance = bullet;
            Rigidbody bulletRigidbody = bullet.GetComponent<Rigidbody>();
            
            //gets the ray where you are pointing to the distance of 

            Vector3 direction = (rayView.GetPoint(10000.0f) - bullet.transform.position).normalized;

            Vector3.Reflect(direction, hit.normal.normalized);
           // transform.rotation = Quaternion.Euler(0f, 0f, 3 + (-90f));

            Debug.Log("direction of bullet" + direction);
            //50 for some kind of bullet impulse
           //f Vector3 directionSpeed = (direction * bulletSpeed)
            bulletRigidbody.AddForce(direction * bulletSpeed, ForceMode.Impulse);
            
            // var projectileObj = Instantiate(bullet, firePoint.position, Quaternion.identity) as GameObject;
            //     projectileObj.GetComponent<Rigidbody>().velocity = (ray.direction).normalized * bulletSpeed;
            // TrackBulletTime(bulletRigidbody.gameObject);
            // HandleCameraOnBullet(cam);
            bulletCloneDestruction(bulletCloneInstance.gameObject);

        }
        else
        {
            destination = ray.GetPoint(1000);
        }
    }

   /* public void forceShotOnDireciton()
    {
        Ray rayView = Camera.main.ScreenPointToRay(Input.mousePosition);

        //crosshair.gameObject.transform.position = cam.WorldToScreenPoint()
        Rigidbody bulletRigidbody = bulletCloneInstance.GetComponent<Rigidbody>();

        //gets the ray where you are pointing to the distance of 

        Vector3 direction = (rayView.GetPoint(10000.0f) - bullet.transform.position).normalized;

        //Vector3.Reflect(direction, hit.normal);
        // transform.rotation = Quaternion.Euler(0f, 0f, 3 + (-90f));

        Debug.Log("direction of bullet" + direction);
        //50 for some kind of bullet impulse
        bulletRigidbody.AddForce(direction * 30, ForceMode.Impulse);
    }*/

    private void bulletCloneDestruction(GameObject bulletClone)
    {
        if(bulletClone)
        Destroy(bulletClone,bulletDeathTimer);
       // bullet = null;
    }


    void ShootRayWithBullet()
    {
        Ray ray = Camera.main.ScreenPointToRay(crosshair.transform.position);
        //
        GameObject bullet = Instantiate(this.bullet, FirePoint.transform.position, FirePoint.transform.rotation) as GameObject;
        Rigidbody bulletRigidbody = bullet.GetComponent<Rigidbody>();

        Vector3 direction = (ray.GetPoint(100000.0f) + bullet.transform.position).normalized;
        //50 for some kind of bullet impulse
        bulletRigidbody.AddForce(direction * bulletSpeed, ForceMode.Impulse);
    }

    void ShootAProjectileWithReflectDirection()
    {

    }
   /* void InstantiateProjectile(Transform firePoint)
    {
        //point to where I want to shoot
        Ray ray = Camera.main.ScreenPointToRay(crosshair.transform.position);

        // Ray hit something in scene
        // Debug.Log("Ray hit : " + hitInfo.collider.gameObject.name);
        GameObject bullet = Instantiate(this.bullet, FirePoint.transform.position, FirePoint.transform.rotation) as GameObject;
        Rigidbody bulletRigidbody = bullet.GetComponent<Rigidbody>();

        //gets the ray where you are pointing to the distance of 

        Vector3 direction = (ray.GetPoint(10000.0f) - bullet.transform.position).normalized;
       
        
        Debug.Log("direction of bullet" + direction);
        //50 for some kind of bullet impulse
        bulletRigidbody.AddForce(direction*bulletSpeed , ForceMode.Impulse);

        // var projectileObj = Instantiate(bullet, firePoint.position, Quaternion.identity) as GameObject;
        //     projectileObj.GetComponent<Rigidbody>().velocity = (ray.direction).normalized * bulletSpeed;
        // TrackBulletTime(bulletRigidbody.gameObject);
       // HandleCameraOnBullet(cam);
            bulletCloneDestruction(bulletRigidbody.gameObject);
        
    } */
    void InstantiateProjectile(Transform firePoint)
    {
        //point to where I want to shoot
        Ray ray = Camera.main.ScreenPointToRay(crosshair.transform.position);

        // Ray hit something in scene
        // Debug.Log("Ray hit : " + hitInfo.collider.gameObject.name);
        GameObject bullet = Instantiate(this.bullet, FirePoint.transform.position, FirePoint.transform.rotation) as GameObject;
        Rigidbody bulletRigidbody = bullet.GetComponent<Rigidbody>();
        
        //gets the ray where you are pointing to the distance of 

        Vector3 direction = (ray.GetPoint(10000.0f) - bullet.transform.position).normalized;

        transform.rotation = Quaternion.Euler(0f, 0f, 3 + (-90f));

        Debug.Log("direction of bullet" + direction);
        //50 for some kind of bullet impulse
        bulletRigidbody.AddForce(direction *bulletSpeed , ForceMode.VelocityChange);

        // var projectileObj = Instantiate(bullet, firePoint.position, Quaternion.identity) as GameObject;
        //     projectileObj.GetComponent<Rigidbody>().velocity = (ray.direction).normalized * bulletSpeed;
        // TrackBulletTime(bulletRigidbody.gameObject);
       // HandleCameraOnBullet(cam);
            bulletCloneDestruction(bulletRigidbody.gameObject);
        
    }

   
    void TrackBulletTime(GameObject bulletClone)
    {
        Ray ray = new Ray(transform.position, transform.forward);
        RaycastHit hit;
        // bulletClone.AddComponent(typeof(Camera));
        // Camera camPos = bulletClone.GetComponent("Camera") as Camera;
        //  Camera camPos = bulletClone.transform.GetChild(1).GetComponent("Camera") as Camera;
        // camPos.transform.position;
        // HandleCameraOnBullet(camPos);
        //   MainCamera.transform.position = Vector3.Lerp(transform.position, TargetPosition.transform.position, speed * Time.deltaTime);
        //  MainCamera.transform.rotation = Quaternion.Lerp(transform.rotation, TargetPosition.transform.rotation, speed * Time.deltaTime);

        // camPos.transform.position = new Vector3(5, 4, 3);
        if (Physics.Raycast(ray, out hit, Time.deltaTime * 300 + 1f))
        {
            Vector3 desiredPosition = Vector3.Reflect(ray.direction, hit.normal)*100;
           // cam.transform.position = new Vector3(5, 4, 3);
            Vector3 smoothPosition = Vector3.Lerp(transform.position, desiredPosition, smoothSpeed) * bulletSpeed * Time.deltaTime;
            //camPos.transform.position = smoothPosition;
        }
    }

    void HandleCameraOnBullet(Camera cameraPosition)
    {
        
        
        Vector3 targetDirection = Vector3.zero;
        // 2 Vertical input
       
        targetDirection = cameraPosition.transform.forward * inputs.verticalInput;
        targetDirection = targetDirection + cameraPosition.transform.right * inputs.horizontalInput;
        targetDirection.Normalize();
        targetDirection.y = 0;

        if (targetDirection == Vector3.zero)
        {
            targetDirection = transform.forward;
        }

        Quaternion targetRotation = Quaternion.LookRotation(targetDirection);
        Quaternion playerRotation = Quaternion.Slerp(transform.rotation, targetRotation, bulletRottationSpeed * Time.deltaTime);

        transform.rotation = playerRotation;
    }
}
