using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnABullet : MonoBehaviour
{

    public GameObject bullet;
    public Vector3 bulletPosition;
    public Rigidbody theBullet;
    public Transform GunTrigger;
    public InputManager input;
    //Rigidbody bulletInstance;
    public ArrayList bulletItems;

    public float bulletSpeed = 1000f;
    public float verticalInput;
    public float horizontalInput;

    public float shootForce, upwardForce;
    // Start is called before the first frame update
    public float timeBetweenShooting, reloadTime, timeBetweenShots;
    void Start()
    {
        bullet = GetComponent<GameObject>();
    }

    // Update is called once per frame
    void Update()
    {
       // MyInput();
       // bulletCloneDestruction();
    }

    private void MyInput()
    {
       
            /*if (Input.GetKeyDown(KeyCode.Mouse0))
            {

                bulletInstance = Instantiate(theBullet, GunTrigger.position, GunTrigger.rotation) as Rigidbody;
                bulletInstance.AddForce(GunTrigger.forward * bulletSpeed, ForceMode.Acceleration);
                bulletItems.Add(bulletInstance);
            }*/ 
            if (Input.GetKeyDown(KeyCode.Mouse0))
            {

            GameObject shot = GameObject.Instantiate(bullet, transform.position, transform.rotation);
            shot.GetComponent<Rigidbody>().AddForce(transform.forward * bulletSpeed);
            bulletCloneDestruction(shot);
        }
   
    }

    private void bulletCloneDestruction(GameObject shotBullet)
    {
        Debug.Log("GunTrigger position" + GunTrigger.position);
        Debug.Log("Bullet position" + shotBullet.transform.position);
        
        Destroy(shotBullet, 5f);
          
            
        
       // Destroy(this.bulletInstance,3);
    }
}
