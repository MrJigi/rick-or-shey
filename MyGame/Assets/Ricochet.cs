using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ricochet : MonoBehaviour
{
    public LayerMask collisionMask;

    public Transform ball;
    private float speed = 15;
    private float rotSpeed = 800;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        // Move the ball forward
        transform.Translate(Vector3.forward * Time.deltaTime * speed);
        //Rotate the ball
        ball.Rotate(Vector3.up * Time.deltaTime * rotSpeed);
        ball.Rotate(Vector3.down * Time.deltaTime * rotSpeed);
        ball.Rotate(Vector3.left * Time.deltaTime * rotSpeed);
        ball.Rotate(Vector3.right * Time.deltaTime * rotSpeed);

        Ray ray = new Ray(transform.position, transform.forward);
        RaycastHit hit;

        if(Physics.Raycast(ray,out hit,Time.deltaTime * speed + 1f, collisionMask))
        {
            Vector3 reflectDit = Vector3.Reflect(ray.direction, hit.normal);
            float rot = 90 - Mathf.Atan2(reflectDit.z, reflectDit.x) * Mathf.Rad2Deg;
            transform.eulerAngles = new Vector3(0, rot, 0);
        }
    }
}
