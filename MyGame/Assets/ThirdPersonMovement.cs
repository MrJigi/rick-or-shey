using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThirdPersonMovement : MonoBehaviour
{

    public CharacterController controller;

    public float moveSpeed = 5.0f;
    public float rotationSpeed = 280.0f;
    


    float horizontal;
    float vertical;



    public float speed = 6f;
    public float turnSmoothTIme = 0.1f;
   /* public float horizontal;
    public float vertical;*/
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
        float horizontal = Input.GetAxisRaw("Horizontal");
        float vertical = Input.GetAxisRaw("Vertical");
        Vector3 direction = new Vector3(horizontal, 0f, vertical).normalized;

        //Mouse  camera angle done. 

        if (direction.magnitude >= 0.1f)
        {
            Debug.Log("magnitude changed"+ direction);
          //  Vector3 movement = new Vector3(horizontal, 0f, vertical) * speed * Time.deltaTime;
           // transform.Translate(movement, Space.World);
            camRotationOnView();


            // float targetAngle = Mathf.Atan2(direction.x, direction.y) * Mathf.Rad2Deg;
            // transform.rotation = Quaternion.Euler(0f, targetAngle, 0f);
            //controller.Move(direction * speed * Time.deltaTime);

        }

    }


    private void camRotationOnView()
    {
        /*    public float movveSpeed = 5.0f;
        public float rotaionSpeed = 280.0f;

        float horizontal;
        float vertical;*/

        Vector3 moveDirection = Vector3.forward * vertical + Vector3.right * horizontal;

        Vector3 projectedCameraForward = Vector3.ProjectOnPlane(Camera.main.transform.forward, Vector3.up);
        Quaternion rotationToCamera = Quaternion.LookRotation(projectedCameraForward, Vector3.up);

        moveDirection = rotationToCamera * moveDirection;
        Quaternion rotationToMoveDirection = Quaternion.LookRotation(moveDirection, Vector3.up);

        transform.rotation = Quaternion.RotateTowards(transform.rotation, rotationToMoveDirection, rotationSpeed * Time.deltaTime);
        transform.position += moveDirection * moveSpeed * Time.deltaTime;


    }

    public void OnMoveInput(float horizontal,float vertical)
    {
        this.vertical = vertical;
        this.horizontal = horizontal;
        Debug.Log($"Player Controller: Move Input: {vertical} , {horizontal}");
    }

/*    private void movingPlayer()
    {
        horizontal = Input.GetAxisRaw("Horizontal");
        vertical = Input.GetAxisRaw("Vertical");
        Vector3 direction = new Vector3(horizontal, 0f, vertical).normalized;

        //Mouse  camera angle done. 

        if (direction.magnitude >= 0.1f)
        {
            Vector3 movement = new Vector3(horizontal, 0f, vertical) * speed * Time.deltaTime;
            transform.Translate(movement, Space.World);


            // float targetAngle = Mathf.Atan2(direction.x, direction.y) * Mathf.Rad2Deg;
            // transform.rotation = Quaternion.Euler(0f, targetAngle, 0f);
            //controller.Move(direction * speed * Time.deltaTime);

        }
    }*/
}
