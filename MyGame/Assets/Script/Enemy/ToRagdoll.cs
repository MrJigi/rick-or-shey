using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToRagdoll : MonoBehaviour
{
    public Collider MainCollider;
    public Collider[] AllColliders;

    private void Awake()
    {
        MainCollider = GetComponent<Collider>();
        AllColliders = GetComponentInChildren<Collider[]>(true);
    }

    public void DoRagdoll(bool isRagdol)
    {
        foreach(var collider in AllColliders)
        {
            collider.enabled = isRagdol;
            MainCollider.enabled = !isRagdol;
            GetComponent<Rigidbody>().useGravity = !isRagdol;
            GetComponent<Animator>().enabled = !isRagdol;
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
