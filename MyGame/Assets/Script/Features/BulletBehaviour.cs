using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletBehaviour : MonoBehaviour
{
    public GameObject bullet;
    public Material textureOfBeam;
    
    // Start is called before the first frame update
    Vector3 pos, dir;
    LaserPoint laserPointing;
    GameObject laserObject;
    LineRenderer laser;
    List<Vector3> laserIndices = new List<Vector3>();
    // Start is called before the first frame update

    public void LaserPoint(Vector3 pos, Vector3 dir, Material material)
    {
        this.laser = new LineRenderer();
        this.laserObject = new GameObject();
        this.laserObject.name = "Laser Beam";
        this.pos = pos;
        this.dir = dir;

        this.laser = this.laserObject.AddComponent(typeof(LineRenderer)) as LineRenderer;
        this.laser.startWidth = 0.1f;
        this.laser.endWidth = 0.1f;
        this.laser.material = material;
        this.laser.startColor = Color.green;

        CastRay(pos, dir, laser);

    }

    void CastRay(Vector3 pos, Vector3 dir, LineRenderer laser)
    {
        laserIndices.Add(pos);
        Ray ray = new Ray(pos, dir);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, 30, 1))
        {
            CheckHit(hit, dir, laser);
            /*laserIndices.Add(hit.point);
            UpdateLaser();*/
        }
        else
        {
            laserIndices.Add(ray.GetPoint(30));
            UpdateLaser();
        }
    }

    void UpdateLaser()
    {
        int count = 0;
        laser.positionCount = laserIndices.Count;
        foreach (Vector3 index in laserIndices)
        {
            laser.SetPosition(count, index);
            count++;
        }
    }

    void CheckHit(RaycastHit hitInfo, Vector3 direction, LineRenderer laser)
    {
        if (hitInfo.collider.gameObject.tag == "Walls")
        {
            Vector3 position = hitInfo.point;
            Vector3 dir = Vector3.Reflect(direction, hitInfo.normal);

            CastRay(position, dir, laser);
        }
        else
        {
            laserIndices.Add(hitInfo.point);
            UpdateLaser();
        }
    }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
      //  castAction();
        //laserPointing = new LaserPoint(gameObject.transform.position, gameObject.transform.forward, textureOfBeam);
    }

    void castAction()
    {
        Vector3 direction = (bullet.transform.position * 5 *Time.deltaTime).normalized;
        Rigidbody bulletRigidbody = bullet.GetComponent<Rigidbody>();
        // bullet.transform.position = (direction);
        bulletRigidbody.AddForce(bullet.transform.position+ new Vector3(0,0,1));
        //Rigidbody bulletRigidbody = bullet.GetComponent<Rigidbody>();
        bulletRigidbody.MovePosition(direction);
        /*   Vector3 direction = (bullet.transform.position*5).normalized;
           Rigidbody bulletRigidbody = bullet.GetComponent<Rigidbody>();
           direction.(bullet.transform.position);
           //bulletRigidbody.AddForce(direction, ForceMode.Impulse);
           Vector3.Reflect(direction, Vector3.right);*/
    }

    void getHitObject()
    {

    }
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.collider.tag == "Walls")
        {
            GameObject collisionPosition = collision.collider.gameObject;
            Vector3 targetDirection = collisionPosition.transform.position - gameObject.transform.position;
            float angle = Vector3.Angle(targetDirection, transform.forward);
            Debug.Log("Current angle is "+angle);
        }
    }
}
