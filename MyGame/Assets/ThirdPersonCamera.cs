using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThirdPersonCamera : MonoBehaviour
{
    public float Speeds = 6.0f;
    public float MotionSpeed = 1f;
    public Transform CameraTransformTarget, Player;
    float mouseX, mouseY;
    private Vector3 moveDirection = Vector3.zero;

    public static ShootingMechanics shootingMechanics;
    //camera snap to movement var
    public Camera cameraControl;
    private float forwardInput;
    private float rightInput;

    private Vector3 velocity;

    // Start is called before the first frame update
    void Start()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void Update()
    {
        CamControl();
    }

    void CamControl()
    {
        mouseX += Input.GetAxis("Mouse X") * MotionSpeed;
        mouseY += Input.GetAxis("Mouse Y") * MotionSpeed;
        mouseY = Mathf.Clamp(mouseY, -90, 90);


        //Camera.main.transform.forward.y;
        moveDirection = new Vector3(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"), 0);
        moveDirection = CameraTransformTarget.TransformDirection(moveDirection);
        
        moveDirection *= Speeds;
        transform.LookAt(CameraTransformTarget);

        CameraTransformTarget.rotation = Quaternion.Euler(-mouseY, mouseX, 0);
       // CameraTransformTarget.rotation = Quaternion.Euler(0, mouseX, 0);
      //  CameraTransformTarget.rotation = Quaternion.Euler(mouseY, 0, 0);
      /*  if (Input.GetKey(KeyCode.LeftShift))
        {
            CameraTransformTarget.rotation = Quaternion.Euler(mouseY, mouseX, 0);

        }
        else
        {
            CameraTransformTarget.rotation = Quaternion.Euler(mouseY, mouseX, 0);
            CameraTransformTarget.rotation = Quaternion.Euler(0, mouseX, 0);
        }*/


        /* Target.rotation = Quaternion.Euler(mouseY, mouseX, 0);
         Player.rotation = Quaternion.Euler(0, mouseX, 0);*/

        //Walking towards camera

       /* moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
        moveDirection = cameraMove.TransformDirection(moveDirection);
        moveDirection *= 6.0f;*/
    }

   

    public void moveToCamera(float forward, float right)
    {
        forwardInput = forward;
        rightInput = right;
        Vector3 camFwd = cameraControl.transform.forward;
        Vector3 camRight = cameraControl.transform.right;
        

        Vector3 translation = forward * cameraControl.transform.forward;
        translation += right * cameraControl.transform.right;
        translation.y = 0;
        
        if (translation.magnitude > 0)
        {
            velocity = translation;
        }
        else
        {
            velocity = Vector3.zero;
        }
    }

    
}
